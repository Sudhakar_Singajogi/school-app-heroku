const md5 = require("md5");
const path = require("path");
const schoolModel = require(path.resolve("src/modules/schools/School"));
const Utils = require(path.resolve("src/config/utils"));
const { Op } = require("sequelize");
const userServ =  require(path.resolve("src/modules/users/services"));

module.exports = {
  createSchool: async (schoolBody) => {
    const { identity, email } = schoolBody;

    const schoolExists = await schoolModel
      .count({
        where: {
          [Op.or]: [{ identity }, { email }],
        },
        logging: (sql, queryObject) => {
          Utils.loglastExecuteQueryToWinston("School Exists", sql);
        },
      })
      .catch((err) => {
        return Utils.catchError("School", err);
      });

    if (schoolExists > 0) {
      return false;
    }

    try {
      const createdSchool = await schoolModel.create({
        ...schoolBody,
        logging: (sql, queryObject) => {
          Utils.loglastExecuteQueryToWinston("Signup new school", sql);
        },
      });

      if(createdSchool) { 
        //create a user for this school to login
        var resultSet = await userServ.createUser(
          {
            "userName":schoolBody.primaryContactPerson,
            "password": md5(await Utils.generateRandomString(6)),
            "roleId": 1,
            "email": schoolBody.email,
            "schoolId": createdSchool.schoolId
          }
        );

       return  await Utils.returnResult(
          "Signup School",
          await schoolModel.findOne({
            where: { schoolId: createdSchool.schoolId },
            include: [],
            attributes: {
              exclude: ["createdAt", "updatedAt"],
            },
          })
        )

      } else {
        return await Utils.returnResult("school creation", schoolModel, false);
      }
      
      /*
      return createdSchool
        ? await Utils.returnResult(
            "Signup School",
            await schoolModel.findOne({
              where: { schoolId: createdSchool.schoolId },
              include: [],
              attributes: {
                exclude: ["createdAt", "updatedAt"],
              },
            })
          )
        : await Utils.returnResult("school creation", schoolModel, false);
        */
    } catch (err) {
      return Utils.catchError("School Installation", err);
    }
  },
};
