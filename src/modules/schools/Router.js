const express = require("express");
const router = express.Router();
const path = require("path");
const { joiMiddleware } = require(path.resolve("src/initializer/framework"));
const Utils = require(path.resolve("src/config/utils"));
const { schoolSchema, createSchoolSchema} = require(path.resolve("src/modules/schools/Schema"));
const schoolService = require(path.resolve("src/modules/schools/services"));

router.get("/", async (req, res, next) => {
  console.log("hey will fetch schools for you");
});

// router.post("/create", joiMiddleware('createSchoolSchema', createSchoolSchema), async (req, res, next) => {
  router.post("/create", async (req, res, next) => {
  // let resultSet = {
  //     message:"School signup request",
  //     result:[],
  //     totalRows:0
  // };
  // console.log('req:', req) 
  var resultSet = await schoolService.createSchool(req.body);

  await Utils.retrunResponse(res, resultSet);
});

module.exports = router;
