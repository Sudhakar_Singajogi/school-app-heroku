const express = require("express");
const router = express.Router();
const path = require("path");
const { joiMiddleware } = require(path.resolve("src/initializer/framework"));
const Utils = require(path.resolve("src/config/utils"));

// const { installationSchema } = require("./Schema");

const {
  jwtAuthenticate,
  jwtAuthorise,
} = require(path.resolve("src/services/jwt-auth-authorize"));
const { validateUserLogin, createUser, resetPassword, passwordresetcode } =  require(path.resolve("src/modules/users/Schema"));
const userServ =  require(path.resolve("src/modules/users/services"));

router.get(
  "/school/:id",
  jwtAuthorise("schools", "read"),
  async (req, res, next) => {
    const schoolId = req.params.id;
    var resultSet = await userServ.getUsers(schoolId);
    await Utils.retrunResponse(res, resultSet);
  }
);

router.post(
  "/login",
  joiMiddleware(validateUserLogin),
  jwtAuthenticate(),
  async (req, res, next) => {}
);

router.post(
  "/create",
  joiMiddleware(createUser), 
  async (req, res, next) => {
  var resultSet = await userServ.createUser(req.body);
  await Utils.retrunResponse(res, resultSet);
  }
);


router.post(
  "/sendresetcode",
  joiMiddleware(passwordresetcode),
  async (req, res) => {
    const email = req.body.email;
    var resultSet = await userServ.sendpasswordresetcode(email);
    await Utils.retrunResponse(res, resultSet);
  }
);

router.patch("/resetpassword", joiMiddleware(resetPassword), async(req, res) => {
  var resultSet = await userServ.resetPassword(req.body);
  await Utils.retrunResponse(res, resultSet);
})

module.exports = router;
