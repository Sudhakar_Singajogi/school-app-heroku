const path = require("path");
const Utils = require(path.resolve("src/config/utils"));

// const userModel = require("./User");
// const User = require("./User");
// const School = require("../schools/School");
// const Role = require("../roles/Role");

const userModel = require(path.resolve("src/modules/users/User"));
const ResetPwdCodeModel = require(path.resolve("src/modules/users/ResetPasswordCodes"));
const User = require(path.resolve("src/modules/users/User"));
const School = require(path.resolve("src/modules/schools/School"));
const Role = require(path.resolve("src/modules/roles/Role"));
const emailtemplates = require(path.resolve("src/emailtemplates/templates"));
const { Op } = require("sequelize");
const md5 = require("md5");

const errHandler = (err) => {
  console.log("Error:", err);
};

//define associations to fetch data
User.belongsTo(School, {
  through: "schoolId",
  foreignKey: "schoolId",
});

// User.hasMany(User);
User.belongsTo(Role, {
  through: "roleId",
  foreignKey: "roleId",
});

const SchoolAssoc = {
  model: School,
  attributes: {
    exclude: [
      "schoolId",
      "primaryEmail",
      "contactNumber",
      "primaryContactPerson",
      "status",
      "createdAt",
      "updatedAt",
    ],
  },
};

const RoleAssoc = {
  model: Role,
  attributes: {
    exclude: ["roleId", "status", "createdAt", "updatedAt"],
  },
};

async function getuserCountByEmail(email) {
  const totalUsers = userModel
    .count({
      where: {
        email: email,
      },
      logging: (sql, queryObject) => {
        Utils.loglastExecuteQueryToWinston("Get toal users on this email", sql);
      },
    })
    .catch((err) => {
      return Utils.catchError("user", err);
    });

  return totalUsers;
}

module.exports = {
  createUser: async (userBody) => {
    const userExists = await getuserCountByEmail(userBody.email);

    if (userExists > 0) {
      return false;
    }

    const user = await userModel
      .create({
        ...userBody,
        logging: (sql, queryObject) => {
          Utils.loglastExecuteQueryToWinston("create a new user", sql);
        },
      })
      .catch(errHandler);

    if (user) {
      const rsSet = await userModel.findByPk(user.userId, {
        include: [SchoolAssoc, RoleAssoc],
        attributes: {
          exclude: [
            "userId",
            "roleId",
            "schoolId",
            "password",
            "status",
            "createdAt",
            "updatedAt",
          ],
        },
      });
      if (rsSet) {
        // return rsSet;
        return  await Utils.returnResult(
          "Signup School",
          rsSet
        )

      }
    }
  },
  getNumberOfUserByEmail: async (email) => {
    const totalUsers = await getuserCountByEmail(email);
    return totalUsers;
  },

  getUserBy: async (reqBody) => {
    const password = md5(reqBody.password);
    const user = await User.findOne({
      where: { email: reqBody.loginId, password: password, status: 1 },
      include: [SchoolAssoc, RoleAssoc],
      attributes: {
        exclude: ["password", "status", "createdAt", "updatedAt"],
      },
    });
    return user
      ? await Utils.returnResult("user", user)
      : await Utils.returnResult("user", user, "No record found");
  },
  getUsers: async (schoolId) => {
    const user = await User.findAll({
      where: { schoolId: schoolId, status: 1 },
      attributes: {
        exclude: [
          "roleId",
          "schoolId",
          "password",
          "status",
          "createdAt",
          "updatedAt",
        ],
      },
    });
    return user
      ? await Utils.returnResult("user", user)
      : await Utils.returnResult("user", user, "No record found");
  },
  sendpasswordresetcode: async (email) => {
    const user = await Utils.findOne({
      model: userModel,
      excludes: ["password" ],
      fetchRowCond: {
        [Op.or]: [{ email: email }],
      },
    });

    
    if (!user.resultSet) {
      user.ValidationErrors = {
        Error: "Check your entered email",
      };
      return await Utils.returnResult("sendpasswordresetcode", user);
    }
    const userName = user.resultSet.userName;

    let resetPwdCode = await Utils.generateRandomString(6);
    resetPwdCode = Utils.convertStringToUpperLowercase(resetPwdCode);

    const currentTime = Date.now();

    // Set the desired time (24 hours in this example)
    const desiredTime = new Date(currentTime);
    desiredTime.setHours(24, 0, 0, 0);
    const codeExpiry = desiredTime.getTime();

    //store the reset code again the userId
    const resetpwdCodeModelObj = {
      email: email,
      code: resetPwdCode,
      expiryTime: codeExpiry,
    };

    await ResetPwdCodeModel.create({
      ...resetpwdCodeModelObj,
      logging: (sql, queryObject) => {
        Utils.loglastExecuteQueryToWinston(
          `Created reset password code for the user: ${user.userId}`,
          sql
        );
      },
    }).catch(errHandler);

    let pwdresetcode_template = emailtemplates.sendpasswordresetcode;
    pwdresetcode_template = pwdresetcode_template.replace(
      "[user_name]",
      userName
    );
    pwdresetcode_template = pwdresetcode_template.replace(
      "[reset_pwd_code]",
      resetPwdCode
    );

    const mailData = {
      from: "ssr.sudhakar@gmail.com", // sender address
      to: email, // list of receivers
      subject: "Reset password code",
      text: "That was easy!",
      html: pwdresetcode_template,
    };

    await Utils.sendMail(mailData);

    return user
      ? await Utils.returnResult("user", user)
      : await Utils.returnResult("user", user, "No record found");
  },
  resetPassword:async(reqObj) => { 
    const user = await Utils.findOne({
      model: userModel,
      excludes: ["password",],
      fetchRowCond: { email: reqObj.email },
    });

    if (!user.resultSet) {
      user.ValidationErrors = {
        Error: "Invalid email address, please chek your email address",
      };
      return await Utils.returnResult("passwordReset", user);
    }

    //check whether entered code is belongs to this user or not

    const code = await Utils.findOne({
      model: ResetPwdCodeModel,
      excludes: ["createdAt", "updatedAt"],
      fetchRowCond: { email: reqObj.email },
      order: ["resetpwdcodeId", "DESC"],
    });

    console.log('reset code is:', code.resultSet.code);
    console.log('entered code is:', reqObj.code);

    if (code.resultSet.code !== reqObj.code) {
      user.ValidationErrors = {
        Error: "Enter code is not valid",
      };
      return await Utils.returnResult("sendpasswordresetcode", user);
    }

    const setPassword = { password: md5(reqObj.password) };
    const cond = {
      where: { email: reqObj.email },
    };

    const updateResp = await Utils.updateData(userModel, setPassword, cond);

    if (updateResp.error) {
      user.DBErrors = {
        Error: "Failed to reset password kindly contact admin",
      };
    }

    //delete the activation code
    await ResetPwdCodeModel.destroy({
      where: { email: reqObj.email },
    }); 
    return await Utils.returnResult("passwordReset", user);

  }
};
