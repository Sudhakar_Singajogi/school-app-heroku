const Joi = require("joi");
const validateUserLogin = Joi.object().keys({
  loginId: Joi.string().email().required(),
  password: Joi.string().required(),
});

const createUser = Joi.object().keys({
  userName: Joi.string().min(3).max(30).required(),
  email: Joi.string().email().required(),
  password: Joi.string().min(6).max(30).required(),
  roleId: Joi.number().min(1).greater(0).required(),
  schoolId: Joi.number().min(1).greater(0).required(),
});

const resetPassword = Joi.object().keys({
  email: Joi.string().required().email(), 
  code:Joi.string().min(6).max(6).required(),
  password:Joi.string().min(6).max(12).required(),
  confirmPassword: Joi.string()
    .required()
    .valid(Joi.ref('password'))
    .options({
      language: {
        any: {
          empty: 'is required',
        },
        string: {
          any: {
            allowOnly: 'must match password',
          },
        },
      },
    })
    .label('Confirm password'),

})

const passwordresetcode = Joi.object().keys({
  email: Joi.string().email().required(),
})

const schemas = {
  validateUserLogin,
  createUser,
  resetPassword,
  passwordresetcode
};
module.exports = schemas;
